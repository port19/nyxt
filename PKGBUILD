# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Contributor: Dct Mei <dctxmei@yandex.com>
# Contributor: Eric S. Londres <elondres@stevens.edu>
# Contributor: Stefan Husmann <stefan-husmann@t-online.de>
# Contributor: Cillian Berragan <cjberragan@gmail.com>
# Contributor: Felix Golatofski <contact@xdfr.de>

pkgname=nyxt
pkgver=3.11.3
pkgrel=1
pkgdesc='A keyboard-driven web browser designed for power users'
arch=('x86_64')
url='https://nyxt.atlas.engineer'
license=('BSD')
depends=(
  'glibc'
  'hicolor-icon-theme'
  'enchant'
  'glib-networking'
  'gobject-introspection-runtime'
  'gsettings-desktop-schemas'
  'libfixposix'
  'webkit2gtk-4.1'
  'zstd'
)
# If someday Nyxt works with other Lisps, replace 'sbcl' with 'common-lisp'.
# NOTE: sbcl provides both common-lisp and cl-asdf.
makedepends=(
  'git'
  'sbcl'
  'cl-asdf'
)
optdepends=(
  'gstreamer: for HTML5 audio/video'
  'gst-plugins-base: for HTML5 audio/video'
  'gst-plugins-good: for HTML5 audio/video'
  'gst-plugins-bad: for HTML5 audio/video'
  'gst-plugins-ugly: for HTML5 audio/video'
)
options=('!strip' '!makeflags')
source=("$pkgname-$pkgver.tar.xz::https://github.com/atlas-engineer/nyxt/releases/download/$pkgver/nyxt-$pkgver-source-with-submodules.tar.xz")
sha512sums=('b9e99efb137c3abc83ba7ee459b3dea91616c88613a30d8ef701b3b6091c6ed088572b16f9152883e7a66393f80afa094b2b49000ee519cb9e3c0e432957a1d8')
b2sums=('11d85728e0593d2aab0e89c860f47a142ee8db0a4c0bf7c7c6d3e409152735d5ab66c140d413fdadd842738d75a65d02fe402dea5e3b272ec9cd413a2b9d4160')

build() {
  make all
}

package() {
  make PREFIX=/usr DESTDIR="$pkgdir" install

  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" licenses/*
}
